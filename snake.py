# Python 2
# Print "Hello world"
import sys
#Python 3
print ("Hello world")

#gathering information from the user
myname = input("Enter your name:")
print("Hello " +myname)

print ("command line args: "+str(sys.argv))
#print(sys.argv[1])

#using files
#reg ex
#loops
#conditions

peopleFH=open("data.txt","r")
for person in peopleFH:
    #sys.stdout.write(person)
    #print(person.rstrip('\r\n'))
    fields=person.split(',')
    fields[-1]=fields[-1].rstrip("\r\n")
    #print ("Number of elements: "+str(len(fields))+"\tlast element is: "+str(len(fields)-1))
    #print("Name: "+fields[0]+"\t"+"Phone: "+fields[len(fields)-1])
    #if "22" in fields:
    if fields[1] == "22":
        print("new age: " +str(int(fields[1])+1))
        print(type(fields[1])) #determine the type of data stored in variable
        print("Name: "+fields[0]+"\t"+"Car: "+fields[-1])

peopleFH.close();
