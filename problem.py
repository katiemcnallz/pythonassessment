import sys

if len(sys.argv) < 2:
    sys.stderr.write("Thats not how you do it\n")
    sys.exit(1)


print(sys.argv[1])

x=0
while x <= len(sys.argv):
    try:
        print(sys.argv[x])
        input("see something say nothing: ")
        x+=1
    except IndexError:
        print("This was your typical developer issue")
        print("but we handled it :-)\n")
        break
    except KeyboardInterrupt:
        print("dont do that")
        break
    except:
        print("some other error occured")
        print("cleaning up and exiting")
        break

print("successfully reach end of the program - and coffee")
