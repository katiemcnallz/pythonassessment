import re

peopleFH=open("data.txt","r")
ptn=re.compile("^.*ati. ", re.IGNORECASE) #what i want to look for

for person in peopleFH:
    #if re.search("22",person):
    if ptn.search(person):
        print(person.rstrip("\r\n")) #show me the line

peopleFH.seek(0,0)

for person in peopleFH:
    #matched=re.search("^.*ati. ",person)
    matched=ptn.search(person)
    if matched:
        print("matched at",matched.span()) #where did i find 22
        print("words found:", matched.group()) #what i found
